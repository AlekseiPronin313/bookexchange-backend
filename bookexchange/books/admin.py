from books.models import Author, Book, BookCard, Favorites, Genre
from django.contrib import admin

admin.site.register(Author)
admin.site.register(Book)
admin.site.register(BookCard)
admin.site.register(Favorites)
admin.site.register(Genre)
